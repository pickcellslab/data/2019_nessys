# README

This repository contains data files and R scripts which have been used to generate the figures of our 2019 article "Nessys: a new set of tools for the automated detection of nuclei within intact tissues
and dense 3D cultures".

Files are grouped within folders named after the figure they refer to. See the Readme file within individual folders for a description of each folder content.