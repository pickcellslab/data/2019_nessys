bbplot <- function(df, cat, var,
                   categoryName, variableName,
                   colorCategory=NULL, color_vector=NULL, 
                   fig,
                   pngWidth=11, pngHeight=11, lims=NULL, dotted=NULL,
                   cx=2, sz=3,
                   x_axis=T){
  
  # Load the required package
  require(ggplot2)
  require(ggbeeswarm)
  
  
  
  # Creates the output folder if it does not already exists
  # NB: generates warning if the folder already exists.
  wd<-getwd()
  path <- paste("/", fig, "/", sep="")
  dir.create(file.path(wd, fig))
  
  
  font_size <- 20
  labels <- levels(factor(df[,cat]))
  
  
  
  if(!is.null(colorCategory)){
    if(is.null(color_vector))
      color_vector<-c("firebrick3", "orange", "black")
    
    names(grad) <- levels(df[,colorCategory])
    Rep_Scale <- scale_fill_manual(name = "", values = color_vector)
  }
  
  if(!is.null(colorCategory)){ plot <-  ggplot(df, aes(x=get(cat), y=get(var), fill=get(colorCategory)))}
  else{plot <-  ggplot(df, aes(x=get(cat), y=get(var)))}
  
  
  # setup theme to show or not show the x axis
  if(x_axis)
    theme <-theme(plot.title= element_text(
      hjust= 0.5, size= font_size,
      family= "Cambria",
      face= "bold"),
      text= element_text(size= font_size, family= "Cambria"),
      axis.text.x=element_text(size=font_size,  hjust=0.5),
      legend.position= "right",
      legend.text.align=0) 
  else
    theme <-theme(plot.title= element_text(
      hjust= 0.5, size= font_size,
      family= "Cambria",
      face= "bold"),
      text= element_text(size= font_size, family= "Cambria"),
      axis.text.x = element_blank(),
      axis.ticks.x = element_blank(),
      legend.position= "right",
      legend.text.align=0) 
  
  
  
  plot <- 
    plot +
    geom_beeswarm(cex=cx, size=sz, shape = 21, alpha = 0.7) + 
    geom_boxplot(outlier.size= 0, outlier.colour= NA, fill= NA, size= 0.2) +
    scale_x_discrete(name = "", labels=labels) +
    scale_y_continuous(name= variableName) +
    theme_bw() +
    theme 
  
  if(!is.null(colorCategory)){
    plot <- plot + Rep_Scale
  }
  
  if(!is.null(dotted)){
    plot <- plot +
      geom_hline(yintercept=dotted,linetype="dotted")
  }
  
  ggsave(filename=paste(wd, path, cat, "_", variableName, "_bbplot.png",sep=""), device="png", type="cairo", units="cm", width=pngWidth, height=pngHeight, dpi=150, plot=plot)
  
  
}