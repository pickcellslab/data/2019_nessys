# REAMDE

This folder contains data and scripts used to generate the graphs in figure 7 of the article.
The data and scripts provided here also make it possible to generate additional graphs not included in the article. 

Data tables are located in the 'data' subfolder. The 5 .tsv files have been generated using the 'Neighbour Exchange' module in [PickCells](https://pickcellslab.frama.io/docs/).

A description of the module and the type of data it generates are described at https://framagit.org/pickcellslab/pickcells-essentials/tree/develop/pickcells-tracking

R scripts are located in the 'scripts' folder.
