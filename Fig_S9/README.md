# README

This folder contains a libre office file giving individual measurements underlying the summary panels in Figure S9.

The table contain accuracy measures generated with the Accuracy comparator module of Nessys to test the accuracy of Nessys segmentation results on datasets obtained from the Broad Bioimage Benchmark Collection (Ljosa et al., 2012) or from (Stegmaier et al., 2016)

The definition of accuracy features are given in Table S3 of the article. 
Computed values are also provided for the number of false positive (FP), false negative (FN), true positive (TP), precision, recall, F-measure as well as number of topological errors.
