# README

This folder contains a libre office file giving individual measurements underlying the summary panels in Figure S7.

The table contains accuracy measures generated with the Accuracy comparator module of Nessys for increasing z-step sizes.

The definition of accuracy features are given in Table S3 of the article. 
Computed values are also provided for the number of false positive (FP), false negative (FN), true positive (TP), precision, recall, F-measure as well as number of topological errors.
