# README

This folder contains the raw data used to generate Fig 5 A and B scatter plots.

  * fig5.tsv: table of nuclei features for the entire E8.75 embryo using the Nessys segmentation as input. Features are described further below
  * fig5.R : R script used to generate the scatter plots in Fig5 (requires the library ggplot2)


Features description:

  * X: X coordinate of the nucleus centroid
  * Y: Y coordinate of the nucleus centroid
  * Z: Z coordinate of the nucleus centroid
  * Mean.Tcf15 : Mean nuclear intensity of the Tcf15 signal
  * Mean.Dapi : Mean nuclear intensity of the Dapi signal
  * Mean.Lamin : Mean nuclear intensity of the Lamin signal
  * Median.Tcf15 : Median nuclear intensity of the Tcf15 signal
  * Median.Dapi : Median nuclear intensity of the Dapi signal
  * Median.Lamin : Median nuclear intensity of the Lamin signal
  * Volume : Nuclear volume in cube micrometer
  * Label : Unique intensity value in the Nessys result image
  * Region_Label : Name of the region where the nucleus is located
